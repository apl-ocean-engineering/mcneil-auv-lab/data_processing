import datetime
import enum
from matplotlib.ticker import FuncFormatter
import matplotlib.pyplot as plt
import numpy as np
import os


class Payload(enum.Enum):
    SEEPS = 1
    SEEPS2 = 2
    GASPOD = 3  # Labsea
    BSEE = 4


Sensors = enum.Enum(
    "Sensors",
    [
        "GTD",
        "INA260",  # Power monitor on BSEE ODroid stack
        "METS",  # METS sensor using RS232 driver
        "METS_VOLTAGE",  # METS sensor using 5V output to ADC
        "PAR",
        "SBE43",  # voltage & oxygen
        "UVILUX",
    ],
)


gtd_fields = [
    "timestamp",
    "mems1_c1",
    "mems1_c2",
    "mems1_c3",
    "mems1_c4",
    "mems1_c5",
    "mems1_c6",
    "mems1_raw_temp",
    "mems1_raw_pressure",
    "mems1_temp",
    "mems1_pressure",
    "unused0",
    "mems2_c1",
    "mems2_c2",
    "mems2_c3",
    "mems2_c4",
    "mems2_c5",
    "mems2_c6",
    "mems2_raw_temp",
    "mems2_raw_pressure",
    "mems2_temp",
    "mems2_pressure",
    "v1",
    "supply_voltage",
    "unused1",
    "unused2",
    "unused3",
]
gtd_field_idxs = {field: idx for idx, field in enumerate(gtd_fields)}

# ros_time percent_humidity temperature_C pressure_Pa
htp_fields = ["timestamp", "humidity", "temperature", "pressure"]
htp_field_idxs = {field: idx for idx, field in enumerate(htp_fields)}


# ros_time latitude longitude depth_m thruster_rpm mode
remus_fields = [
    "timestamp",
    "latitude_deg",
    "longitude_deg",
    "depth_m",
    "thruster_rpm",
    "mode",
    "heading_deg",
]
remus_field_idxs = {field: idx for idx, field in enumerate(remus_fields)}


def load_mets_voltage(logdir, mission):
    concentration, temperature, reference = [], [], []
    concentration_datafile = os.path.join(
        logdir, f"{mission}_mets_concentration_voltage.txt"
    )
    temperature_datafile = os.path.join(
        logdir, f"{mission}_mets_temperature_voltage.txt"
    )
    reference_datafile = os.path.join(logdir, f"{mission}_mets_reference_voltage.txt")
    for data, datafile in zip(
        [concentration, temperature, reference],
        [concentration_datafile, temperature_datafile, reference_datafile],
    ):
        try:
            for line in open(datafile, "r"):
                if "#" in line:
                    print(line)
                    continue
                tokens = line.split(" ")
                values = list(map(float, tokens))
                data.append(values)
        except FileNotFoundError:
            print(f"Could not find {datafile}; skipping")
            return

    concentration = np.array(concentration)
    temperature = np.array(temperature)
    reference = np.array(reference)
    return concentration, temperature, reference


def load_gtd(logdir, mission):
    gtd_data = []
    gtd_datafile = os.path.join(logdir, f"{mission}_prooceanus_gtd.txt")
    try:
        for line in open(gtd_datafile, "r"):
            if "#" in line:
                print(line)
                continue
            tokens = line.split(",")
            if len(tokens) != 28:
                # print(line)
                continue
            info = tokens[0]
            tt = float(info.split(" ")[0])
            values = list(map(float, tokens[2:]))
            values.insert(0, tt)
            gtd_data.append(values)
    except FileNotFoundError:
        print("Could not find GTD data; skipping")

    gtd_data = np.array(gtd_data)
    return gtd_data


def load_sbe43_oxygen(logdir, mission):
    datafile = os.path.join(logdir, f"{mission}_sbe43_dissolved_oxygen.txt")
    data = []
    try:
        for line in open(datafile, "r"):
            if "#" in line:
                print(line)
                continue
            tokens = line.split(" ")
            values = list(map(float, tokens))
            data.append(values)
    except FileNotFoundError:
        print("Could not find SBE dissolved oxygen data; skipping")

    data = np.array(data)
    return data


def load_sbe43_voltage(logdir, mission, ref=False):
    """
    Columns are:
    ros_time voltage max_counts c0 c1 c2 ... cN
    """
    if ref:
        datafile = os.path.join(logdir, f"{mission}_sbe43_ref_voltage.txt")
    else:
        datafile = os.path.join(logdir, f"{mission}_sbe43_voltage.txt")
    data = []
    try:
        for line in open(datafile, "r"):
            if "#" in line:
                print(line)
                continue
            tokens = line.split(" ")
            values = list(map(float, tokens))
            data.append(values)
    except FileNotFoundError:
        print("Could not find SBE voltage data; skipping")

    data = np.array(data)
    return data


def load_bme280(logdir, mission):
    datafile = os.path.join(logdir, f"{mission}_bme280_htp.txt")
    data = []
    for line in open(datafile, "r"):
        if "#" in line:
            print(line)
            continue
        tokens = line.split(" ")
        values = list(map(float, tokens))
        data.append(values)
    data = np.array(data)
    return data


def load_ctd(logdir, mission):
    datafile = os.path.join(logdir, f"{mission}_recon_ctd.txt")
    data = []
    try:
        for line in open(datafile, "r"):
            if "#" in line:
                print(line)
                continue
            tokens = line.split(" ")
            values = list(map(float, tokens))
            data.append(values)
    except FileNotFoundError:
        print("Could not find Remus CTD data; skipping")

    data = np.array(data)
    return data


def load_recon(logdir, mission):
    datafile = os.path.join(logdir, f"{mission}_recon_state.txt")
    data = []
    for line in open(datafile, "r"):
        if "#" in line:
            print(line)
            continue
        tokens = line.split(" ")
        values = list(map(float, tokens))
        data.append(values)
    data = np.array(data)
    return data


def load_power_channel(logdir, mission):
    datafile = os.path.join(logdir, f"{mission}_power_status.txt")
    data = None
    for line in open(datafile, "r"):
        if "#" in line:
            print(line)
            continue
        tokens = line.split(",")
        tt = float(tokens[0])
        # If this is the first line, initialize with field names
        if data is None:
            data = {}
            channels = []
            for ii in range(1, len(tokens), 2):
                channels.append(tokens[ii])
                print(f"Power channels: {channels}")
            for channel in channels:
                data[channel] = []
        for ii in range(1, len(tokens), 2):
            channel = tokens[ii]
            value = int(tokens[ii + 1])
            data[channel].append((tt, value))
    for key, val in data.items():
        data[key] = np.array(val)
    return data


def load_ina260(logdir, mission):
    datafile = os.path.join(logdir, f"{mission}_ina260_power.txt")
    return load_data(datafile, "power monitor")


def load_par(logdir, mission):
    datafile = os.path.join(logdir, f"{mission}_par_irradiance.txt")
    return load_data(datafile, "PAR")


def load_uvilux(logdir, mission):
    datafile = os.path.join(logdir, f"{mission}_uvilux_fluorescence.txt")
    return load_data(datafile, "UviLux")


def load_data(datafile, description):
    """
    Loads generic datafile; works for any of the files with a constant
    number of columns (e.g. not lights or power monitor)
    """
    data = []
    try:
        for line in open(datafile, "r"):
            if "#" in line:
                print(line)
                continue
            tokens = line.split(" ")
            values = list(map(float, tokens))
            data.append(values)
    except FileNotFoundError:
        print(f"Couldn't load {description} data")
    data = np.array(data)
    return data


def load_sealite_status(logdir, mission):
    datafile = os.path.join(logdir, f"{mission}_sealite_status.txt")
    data = None
    try:
        for line in open(datafile, "r"):
            if "#" in line:
                print(line)
                continue
            tokens = line.split(",")
            tt = float(tokens[0])
            # If this is the first line, initialize with field names
            if data is None:
                data = {}
                lights = []
                for ii in range(1, len(tokens), 4):
                    lights.append(tokens[ii])
                    print(f"Installed lights: {lights}")
                for light in lights:
                    data[light] = []
            for ii in range(1, len(tokens), 4):
                light = tokens[ii]
                address = int(tokens[ii + 1])
                temperature = float(tokens[ii + 2])
                level = float(tokens[ii + 3])
                data[light].append((tt, address, temperature, level))
        for key, val in data.items():
            data[key] = np.array(val)
    except FileNotFoundError:
        print("Could not find Sealite status data; skipping")
    return data


def set_ax_color(ax, color, which):
    """Set axis label / ticks / legend to given color"""
    ax.tick_params(which="both", color=color, labelcolor=color)
    if which == "x":
        ax.xaxis.label.set_color(color)
    else:
        ax.yaxis.label.set_color(color)


class DivePlotter:

    # The depth plots are GTD, SBE43 Oxygen, Remus CTD. One axis each
    # The time series are Remus heading/depth, Remus temp/conductivity, GTD, SBE43 Oxygen, housing, power channels

    payload_sensors = {
        # Assume we always have Remus + HTP + odroid power channels
        Payload.BSEE: (Sensors.INA260, Sensors.PAR, Sensors.UVILUX),
        # Run both in LABSEA (both vehicles) and SEEPS (legacy)
        Payload.GASPOD: (Sensors.GTD, Sensors.SBE43),
        Payload.SEEPS: (Sensors.METS,),
        Payload.SEEPS2: (Sensors.METS_VOLTAGE,),
    }

    def __init__(self, mission: str, payload: Payload) -> None:
        """
        Assumes that the script will be run in the directory
        where plots should be generated.

        * mission: used to prefix + label the plots
        """
        self.mission = mission
        if payload not in [Payload.BSEE, Payload.GASPOD]:
            print(f"DivePlotter NYI for Payload {payload}")
        self.sensors = DivePlotter.payload_sensors[payload]
        logdir = "extracted"

        # Go ahead and try load all of 'em; will return [] on empty file
        # Remus + core ODROID
        self.htp_data = load_bme280(logdir, mission)
        self.ctd_data = load_ctd(logdir, mission)
        self.remus_data = load_recon(logdir, mission)
        self.power_channel_data = load_power_channel(logdir, mission)
        # LABSEA / GASPOD
        self.gtd_data = load_gtd(logdir, mission)
        self.oxygen_data = load_sbe43_oxygen(logdir, mission)
        self.voltage_data = load_sbe43_voltage(logdir, mission)
        self.ref_voltage_data = load_sbe43_voltage(logdir, mission, ref=True)
        # BSEE
        self.sealite_data = load_sealite_status(logdir, mission)
        self.power_data = load_ina260(logdir, mission)
        self.par_data = load_par(logdir, mission)
        self.uvilux_data = load_uvilux(logdir, mission)
        # SEEPS2
        (
            self.mets_concentration_data,
            self.mets_temperature_data,
            self.mets_reference_data,
        ) = load_mets_voltage(logdir, mission)
        print("loaded mets data!")
        print(self.mets_concentration_data)

    def plot_dive(self):
        self.plot_data_depth(self.mission, self.sensors)
        self.plot_data_time(self.mission, self.sensors)

    def interpolate_depth(self, posix_times):
        remus_depth_idx = remus_field_idxs["depth_m"]
        dd = np.interp(
            posix_times, self.remus_data[:, 0], self.remus_data[:, remus_depth_idx]
        )
        return dd

    def plot_data_depth(self, mission: str, payload: "set[int]") -> None:
        """
        plot science values as a function of depth

        """
        fig = plt.figure(figsize=(15, 10))

        num_axes = 1  # All payloads want to see data from Remus CTD plots
        if Sensors.GTD in payload:
            num_axes += 1
        if Sensors.SBE43 in payload:
            num_axes += 1

        axes = fig.subplots(1, num_axes)
        try:
            ctd_ax = axes[0]
        except TypeError:
            # If num_axes=1, subplots returns an ax rather than an array
            ctd_ax = axes
        curr_idx = 1
        if Sensors.GTD in payload:
            gtd_ax = axes[curr_idx]
            curr_idx += 1
        if Sensors.SBE43 in payload:
            oxy_ax = axes[curr_idx]
            curr_idx += 1

        # TODO: Add any additional depth plots

        ctd_ax.set_xlabel("Remus Temp. (C)")
        ctd_ax.set_ylabel("Depth (m)")
        ctd_ax_cond = ctd_ax.twiny()
        ctd_ax_cond.set_xlabel("Remus Conductivity")
        set_ax_color(ctd_ax, "black", "x")
        set_ax_color(ctd_ax_cond, "red", "x")
        try:
            ctd_depth = self.interpolate_depth(self.ctd_data[:, 0])
            cond_idx = 1
            temp_idx = 2
            ctd_ax.plot(
                self.ctd_data[:, temp_idx],
                ctd_depth,
                "k.",
                markersize=2,
                label="Temperature",
            )
            ctd_ax_cond.plot(
                self.ctd_data[:, cond_idx],
                ctd_depth,
                "r.",
                markersize=2,
                label="Conductivity",
            )
        except Exception as ex:
            print("Unable to plot recon CTD data!")
            print(ex)

        # plot science values as a function of time

        if Sensors.GTD in payload:
            gtd_ax.set_xlabel("GTD MEMS pressure (mbar)")
            gtd_ax.set_ylabel("Depth (m)")
            try:
                gtd_depth = self.interpolate_depth(self.gtd_data[:, 0])
                gtd1_idx = gtd_field_idxs["mems1_pressure"]
                gtd2_idx = gtd_field_idxs["mems2_pressure"]
                gtd_ax.plot(self.gtd_data[:, gtd1_idx], gtd_depth, "k.", label="mems1")
                gtd_ax.plot(self.gtd_data[:, gtd2_idx], gtd_depth, "r.", label="mems2")
                gtd_ax.legend()
            except Exception as ex:
                print("Unable to plot GTD data!")
                print(ex)

        if Sensors.SBE43 in payload:
            oxy_ax.set_xlabel("Oxygen (mL/L)")
            oxy_ax.set_ylabel("Depth (m)")
            try:
                oxy_depth = self.interpolate_depth(self.oxygen_data[:, 0])
                oxy_im = oxy_ax.scatter(
                    self.oxygen_data[:, 1],
                    oxy_depth,
                    c=self.oxygen_data[:, 0],
                    s=10,
                    ec="none",
                )
                cbar = fig.colorbar(oxy_im, ax=oxy_ax, label="time")
                cbar.ax.yaxis.set_major_formatter(
                    FuncFormatter(
                        lambda x, pos: datetime.datetime.fromtimestamp(x).strftime(
                            "%Y-%m-%d\n%H:%M:%S"
                        )
                    )
                )

            except Exception as ex:
                print("Unable to plot oxygen concentration!")
                print(ex)

            voltage_ax = oxy_ax.twiny()
            voltage_ax.set_xlabel("SBE43 Voltage (V)")
            try:
                voltage_depth = self.interpolate_depth(self.voltage_data[:, 0])
                voltage_ax.plot(
                    self.voltage_data[:, 1], voltage_depth, "k.", markersize=2
                )
            except Exception as ex:
                print("Unable to plot oxygen voltage!")
                print(ex)

        fig.suptitle(mission, fontsize="xx-large")

        # Flip all axes so positive depth is down
        # TODO: Seems like there should be an option for this in creating the axis? flipud?
        try:
            for ax in axes:
                ylim = ax.get_ylim()
                ax.set_ylim((ylim[1], ylim[0]))
        except TypeError:
            ylim = axes.get_ylim()
            axes.set_ylim((ylim[1], ylim[0]))

        fig.savefig(f"{mission}_depth_plots.png", dpi=300)

    def plot_data_time(self, mission: int, sensors: "set[int]"):
        fig = plt.figure(figsize=(10, 15))
        # Everybody wants power channels, remus ctd, odroid htp, remus state
        num_axes = 4
        if Sensors.GTD in sensors:
            num_axes += 1
        if Sensors.SBE43 in sensors:
            num_axes += 1
        if Sensors.METS_VOLTAGE in sensors:
            num_axes += 1

        axes = fig.subplots(num_axes, 1, gridspec_kw={"hspace": 0}, sharex=True)
        curr_ax_idx = 0

        # Depth/heading plot
        state_ax = axes[curr_ax_idx]
        curr_ax_idx += 1
        state_ax2 = state_ax.twinx()
        state_ax.set_ylabel("Depth (m)")
        state_ax2.set_ylabel("Heading (deg)")
        set_ax_color(state_ax, "black", "y")
        set_ax_color(state_ax2, "red", "y")
        try:
            depth_idx = remus_field_idxs["depth_m"]
            state_ax.plot(
                self.remus_data[:, 0], self.remus_data[:, depth_idx], "k.", markersize=2
            )
            ylim = state_ax.get_ylim()
            state_ax.set_ylim([ylim[1], ylim[0]])

            heading_idx = remus_field_idxs["heading_deg"]
            state_ax2.plot(
                self.remus_data[:, 0],
                self.remus_data[:, heading_idx],
                "r.",
                markersize=2,
            )
        except Exception as ex:
            print("Unable to plot Remus heading/depth data!")
            print(ex)

        # GTD plot
        if Sensors.GTD in sensors:
            gtd_ax = axes[curr_ax_idx]
            curr_ax_idx += 1
            gtd_ax.set_ylabel("GTD pressure (mbar)")
            try:
                gtd1_idx = gtd_field_idxs["mems1_pressure"]
                gtd2_idx = gtd_field_idxs["mems2_pressure"]
                gtd_ax.plot(
                    self.gtd_data[:, 0],
                    self.gtd_data[:, gtd1_idx],
                    "k.",
                    markersize=2,
                    label="mems1",
                )
                gtd_ax.plot(
                    self.gtd_data[:, 0],
                    self.gtd_data[:, gtd2_idx],
                    "r.",
                    markersize=2,
                    label="mems2",
                )
                gtd_ax.legend()
            except Exception as ex:
                print("Unable to plot GTD data!")
                print(ex)

        # CTD (from recon)
        ctd_ax = axes[curr_ax_idx]
        curr_ax_idx += 1
        ctd_ax2 = ctd_ax.twinx()
        ctd_ax.set_ylabel("Remus Temp.(C)")
        ctd_ax2.set_ylabel("Remus Conductivity")
        set_ax_color(ctd_ax2, "red", "y")
        set_ax_color(ctd_ax, "black", "y")
        try:
            cond_idx = 1
            temp_idx = 2
            ctd_ax.plot(
                self.ctd_data[:, 0],
                self.ctd_data[:, temp_idx],
                "k.",
                markersize=2,
                label="Temperature",
            )
            ctd_ax2.plot(
                self.ctd_data[:, 0],
                self.ctd_data[:, cond_idx],
                "r.",
                markersize=2,
                label="Conductivity",
            )
        except Exception as ex:
            print("Unable to plot Remus CTD data!")
            print(ex)

        # SBE43 Oxygen
        # Oxygen concentration
        if Sensors.SBE43 in sensors:
            oxy_ax = axes[curr_ax_idx]
            curr_ax_idx += 1
            voltage_ax = oxy_ax.twinx()
            oxy_ax.set_ylabel("Oxygen (mL/L)")
            voltage_ax.set_ylabel("SBE43 voltage (V)")
            set_ax_color(voltage_ax, "red", "y")
            try:
                oxy_ax.plot(
                    self.oxygen_data[:, 0], self.oxygen_data[:, 1], "k.", markersize=2
                )
            except Exception as ex:
                print("Unable to plot oxygen concentration!")
                print(ex)
            try:
                voltage_ax.plot(
                    self.voltage_data[:, 0],
                    self.voltage_data[:, 1],
                    "r.",
                    markersize=2,
                    label="SBE43",
                )
                voltage_ax.plot(
                    self.ref_voltage_data[:, 0],
                    self.ref_voltage_data[:, 1],
                    "b.",
                    markersize=2,
                    label="1.8V",
                )
                voltage_ax.legend()
            except Exception as ex:
                print("Unable to plot oxygen voltage!")
                print(ex)

        # METS plot
        if Sensors.METS_VOLTAGE in sensors:
            ax = axes[curr_ax_idx]
            curr_ax_idx += 1
            ax.set_ylabel("METS voltage")
            try:
                ax.plot(
                    self.mets_concentration_data[:, 0],
                    self.mets_concentration_data[:, 1],
                    "r.",
                    markersize=2,
                    label="concentration",
                )
                ax.plot(
                    self.mets_temperature_data[:, 0],
                    self.mets_temperature_data[:, 1],
                    "k.",
                    markersize=2,
                    label="temperature",
                )
                ax.plot(
                    self.mets_reference_data[:, 0],
                    self.mets_reference_data[:, 1],
                    "b.",
                    markersize=2,
                    label="reference",
                )
                ax.legend()
            except Exception as ex:
                print("Unable to plot METS voltage data!")
                print(ex)

        # Plot HTP; make sure that values are changing for all fields
        htp_ax = axes[curr_ax_idx]
        curr_ax_idx += 1
        temp_ax = htp_ax.twinx()
        pressure_ax = htp_ax.twinx()
        htp_ax.set_ylabel("Housing Humidity (%)")
        temp_ax.set_ylabel("Housing Temperature (C)")
        pressure_ax.set_ylabel("Housing pressure (Pa)")
        set_ax_color(htp_ax, "black", "y")
        set_ax_color(temp_ax, "red", "y")
        set_ax_color(pressure_ax, "blue", "y")
        try:
            humidity_idx = htp_field_idxs["humidity"]
            temperature_idx = htp_field_idxs["temperature"]
            pressure_idx = htp_field_idxs["pressure"]
            htp_ax.plot(
                self.htp_data[:, 0], self.htp_data[:, humidity_idx], "k.", markersize=2
            )
            temp_ax.plot(
                self.htp_data[:, 0],
                self.htp_data[:, temperature_idx],
                "r.",
                markersize=2,
            )
            pressure_ax.plot(
                self.htp_data[:, 0], self.htp_data[:, pressure_idx], "b.", markersize=2
            )
        except Exception as ex:
            print("Unable to plot htp data!")
            print(ex)

        # Power status plot (used to check that the periodic cycling of the
        # pump worked)
        # To actually be legible, we'd have to subsample markers, so using lines instead
        # styles = ['.', 'x', '+', 'v']
        # stylecycler = itertools.cycle(styles)
        power_ax = axes[curr_ax_idx]
        curr_ax_idx += 1
        power_ax.set_ylabel("Power channel status")
        try:
            for channel, data in self.power_channel_data.items():
                power_ax.plot(data[:, 0], data[:, 1], label=channel)
            power_ax.legend()
        except Exception as ex:
            print("Unable to plot power data!")
            print(ex)

        # Style & save plot
        fig.suptitle(mission)

        # Only have bottom ticks / label on bottom axis
        for ax in axes[:-1]:
            ax.xaxis.set_ticklabels([])

        for ax in axes[-1:]:
            # I tried a number of things to rotate the labels, but they all
            # interfered with the FuncFormatter.
            # ax.tick_params(labelrotation=45)
            ax.set_xlabel("Time")
            ax.xaxis.set_major_formatter(
                FuncFormatter(
                    lambda x, pos: datetime.datetime.fromtimestamp(x).strftime(
                        "%Y-%m-%d\n%H:%M:%S"
                    )
                )
            )
        # Add vertical lines
        ticks = axes[-1].get_xticks()
        print(f"Ticks at: {ticks}")
        for ax in axes:
            xlim = ax.get_xlim()
            ymin, ymax = ax.get_ylim()
            ax.vlines(ticks, ymin, ymax, colors="gray", linestyles="dotted")
            ax.set_xlim(xlim)

        # Deal with bad data
        # Use htp since it's included in every stretch limo ODroid section, while
        # other sensors come and go depending on mission.
        htp_times = np.array(self.htp_data[:, 0])
        tmin = np.min(htp_times)
        tmax = np.max(htp_times)
        if tmax - tmin > (24 * 60 * 60):
            print("Dive duration too long -- cropping to most recent day")
            (idxs,) = np.where(tmax - htp_times < 24 * 60 * 60)
            xmin = np.min(htp_times[idxs])
            xmax = np.max(htp_times[idxs])
            for ax in axes:
                ax.set_xlim([xmin, xmax])

        fig.savefig(f"{mission}_time_plots.png", dpi=300)


def plot_dive(mission, payload):
    dive_plotter = DivePlotter(mission, payload)
    dive_plotter.plot_dive()
