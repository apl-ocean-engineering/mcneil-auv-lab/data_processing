# These formatters feel like something that could be auto-generated
# from the message definitions; though I didn't have time to think it
# through for handling arrays, etc.

import os
import rosbag
import subprocess


class Bme280HtpFormatter:
    @staticmethod
    def format_header():
        return "# ros_time percent_humidity temperature_C pressure_Pa\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {} {}\n".format(
            msg.header.stamp.to_sec(), msg.humidity, msg.temperature, msg.pressure
        )


class CtdFormatter:
    @staticmethod
    def format_header():
        return "# ros_time conductivity temperature pressure salinity sound_speed\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {} {} {} {}\n".format(
            msg.header.stamp.to_sec(),
            msg.conductivity,
            msg.temperature,
            msg.pressure,
            msg.salinity,
            msg.sound_speed,
        )


class Ina260PowerFormatter:
    @staticmethod
    def format_header():
        return "# ros_time voltage(V) current(A) power(W)\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {} {}\n".format(
            msg.header.stamp.to_sec(), msg.voltage, msg.current, msg.power
        )


class MetsFormatter:
    @staticmethod
    def format_header():
        return "# ros_time temperature_counts temperature_C methane_counts methane_concentration_umol_L\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {} {} {}\n".format(
            msg.header.stamp.to_sec(),
            msg.temperature_counts,
            msg.temperature,
            msg.methane_counts,
            msg.methane_concentration,
        )


class ParFormatter:
    @staticmethod
    def format_header():
        return "# ros_time irradiance(%) temperature(C)\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {}\n".format(
            msg.header.stamp.to_sec(), msg.irradiance, msg.temperature
        )


class PowerStatusFormatter:
    @staticmethod
    def format_header():
        return "# ros_time channel1 pin1 status1 ... channelN pinN statusN\n"

    @staticmethod
    def format_message(msg):
        # PowerStatus needs to be comma-separated, because some
        # of the fields have space-separated names (d'oh!)
        channel_strings = [
            "{},{}".format(channel, status)
            for channel, status in zip(msg.channels, msg.status)
        ]
        time_string = "{}".format(msg.header.stamp.to_sec())
        channel_strings.insert(0, time_string)
        return ",".join(channel_strings) + "\n"


class RawUtf8DataInFormatter:
    """
    Only print out data from device -> driver

    While RawData.msg allows bytes, this assumes that the bytes are
    used to encode string with utf8

    This makes sense for e.g. the ProOceanus MiniTDGP which spits out
    an enormous number of fields.
    """

    @staticmethod
    def format_header():
        return "# ros_time raw_data_string\n"

    @staticmethod
    def format_message(msg):
        if msg.direction == msg.DATA_OUT:
            return ""
        try:
            return "{} {}\n".format(
                msg.header.stamp.to_sec(), msg.data.decode("utf-8").strip()
            )
        except Exception:
            return ""


class ReconStateFormatter:
    """
    This doesn't attempt to output every field in the V2RState
    message; just the ones relevant for our plotting.
    """

    @staticmethod
    def format_header():
        return "# ros_time latitude longitude depth_m thruster_rpm mode heading\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {} {} {} {} {}\n".format(
            msg.header.stamp.to_sec(),
            msg.latitude,
            msg.longitude,
            msg.depth,
            msg.thruster_rpm,
            msg.mode,
            msg.heading,
        )


class ReconTimeFormatter:
    """
    Format the V2RTime message that will be used for synchronizing between
    clocks on the frontseat and backseat.
    """

    @staticmethod
    def format_header():
        return "# ros_time remus_date_string remus_time_string remus_unixtime\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {} {}\n".format(
            msg.header.stamp.to_sec(), msg.date_string, msg.time_string, msg.unixtime
        )


class RosoutFormatter:
    @staticmethod
    def format_header():
        return "# ros_time: [severity] log_message\n"

    @staticmethod
    def format_message(msg):
        severity = None
        if msg.level == msg.FATAL:
            severity = "FATAL"
        elif msg.level == msg.ERROR:
            severity = "ERROR"
        elif msg.level == msg.WARN:
            severity = "WARN"
        else:
            # Don't output messages that are less than a warning
            return ""

        return "{}: [{}] {}\n".format(msg.header.stamp.to_sec(), severity, msg.msg)


class Sbe43DoFormatter:
    @staticmethod
    def format_header():
        return "# ros_time dissolved_oxygen_mL_L\n"

    @staticmethod
    def format_message(msg):
        return "{} {}\n".format(msg.header.stamp.to_sec(), msg.dissolved_oxygen)


class Ads1115VoltageFormatter:
    @staticmethod
    def format_header():
        return "# ros_time voltage max_counts c0 c1 c2 ... cN\n"

    @staticmethod
    def format_message(msg):
        counts_string = " ".join("{}".format(cc) for cc in msg.counts)
        return "{} {} {} {}\n".format(
            msg.header.stamp.to_sec(), msg.voltage, msg.max_counts, counts_string
        )


class SealiteFormatter:
    @staticmethod
    def format_header():
        return (
            "# ros_time name1 address1 temperature1 level1 ... "
            "nameN addressN temperatureN levelN\n"
        )

    @staticmethod
    def format_message(msg):
        # PowerStatus needs to be comma-separated, because some
        # of the fields have space-separated names (d'oh!)
        status_strings = [
            "{},{},{},{}".format(label, address, temperature, level)
            for label, address, temperature, level in zip(
                msg.light, msg.address, msg.temperature, msg.level
            )
        ]
        time_string = "{}".format(msg.header.stamp.to_sec())
        status_strings.insert(0, time_string)
        return ",".join(status_strings) + "\n"


class UviluxFormatter:
    @staticmethod
    def format_header():
        return "# ros_time fluorescence eht_voltage quality_flag\n"

    @staticmethod
    def format_message(msg):
        return "{} {} {} {}\n".format(
            msg.header.stamp.to_sec(),
            msg.fluorescence,
            msg.eht_voltage,
            msg.quality_flag,
        )


def extract_data(output_filenames, topic_formatters, prefix):
    filenames = os.listdir(".")
    bag_filenames = [ff for ff in filenames if ff.endswith("bag")]
    active_filenames = [ff for ff in filenames if ff.endswith("active")]
    if len(active_filenames) > 0:
        os.makedirs("reindexed", exist_ok=True)
        for ff in active_filenames:
            reindexed_bagfilename = f"reindexed/{ff}"
            if os.path.exists(reindexed_bagfilename):
                print(f"Not reindexing: {reindexed_bagfilename} -- already exists")
            else:
                print("Reindexing: {}".format(ff))
                subprocess.check_call(
                    ["rosbag", "reindex", "--output-dir=reindexed", ff]
                )
            bag_filenames.append(reindexed_bagfilename)
    # Want to process them chronologically, so rely on timestamps being in the filename
    bag_filenames.sort()
    print("Extracting data from bagfiles: \n  {}".format("\n  ".join(bag_filenames)))
    os.makedirs("extracted", exist_ok=True)

    output_files = {
        topic: open("extracted/" + prefix + "_" + filename, mode="w", encoding="utf-8")
        for topic, filename in output_filenames.items()
    }

    for topic, fp in output_files.items():
        fp.write(topic_formatters[topic].format_header())

    for bag_file in bag_filenames:
        if "cam" in bag_file:
            continue
        print(f"extracting {bag_file}")
        with rosbag.Bag(bag_file) as bag:
            for topic, msg, _ in bag.read_messages():
                if topic in output_filenames:
                    fp = output_files[topic]
                    fp.write(topic_formatters[topic].format_message(msg))

    for fp in output_files.values():
        fp.close()
