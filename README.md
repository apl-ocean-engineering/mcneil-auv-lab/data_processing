## data_processing

Various scripts to extract and plot data from the rosbags collected
by the backseat computers on the Remus vehicles.

### Data extraction

Craig needs the raw data in a widely-readable format; bagfiles aren't an
acceptable deliverable format.

The scripts `extract_seeps_ngr`, `extract_seeps_legacy` split the bagfiles
into individual text files, one per topic of interest.

We assume that the script will be called from within the directory containing
the bag file(s) of interest, and will create up to two new directories:
* extracted: contains all .txt files for individual instruments
* reindexed: only created if any of the bagfiles weren't properly closed and have the .active suffix. The script runs rosbag reindex, and puts the result in this directory

