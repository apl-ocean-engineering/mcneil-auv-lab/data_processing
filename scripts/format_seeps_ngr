#! /usr/bin/env python3

import data_processing

output_filenames = {
    "/ngr/recon/V2RTime": "recon_time.txt",
    "/ngr/recon/V2RState": "recon_state.txt",
    "/ngr/power_manager/status": "power_status.txt",
    "/ngr/bme280/htp": "bme280_htp.txt",
    # "/ngr/franatech_mets/mets": "mets.txt",
    "/ngr/mets/concentration_voltage": "mets_concentration_voltage.txt",
    "/ngr/mets/temperature_voltage": "mets_temperature_voltage.txt",
    "/ngr/mets/mets_adc_driver/P2_GND/voltage": "mets_reference_voltage.txt",
    "/rosout": "rosout.txt",
}

# We can't use functools.singledispatch because rosbag.Bag.read_messages
# returns something that quacks like the original message type, but isn't
# actually an instance of it. So, just do a lookup.
# Each formatter will have two functions:
# 1) format_header -- print out the header for the text file
# 2) format_message --  format a single message into a line of text
topic_formatters = {
    "/ngr/recon/V2RTime": data_processing.ReconTimeFormatter,
    "/ngr/recon/V2RState": data_processing.ReconStateFormatter,
    "/ngr/power_manager/status": data_processing.PowerStatusFormatter,
    "/ngr/bme280/htp": data_processing.Bme280HtpFormatter,
    # For SEEPS2, the RS232 output of the METS is broken, so we're using the ADS1115 ADC
    # on the instrument's 5V lines.
    # "/ngr/franatech_mets/mets": data_processing.MetsFormatter,
    "/ngr/mets/concentration_voltage": data_processing.Ads1115VoltageFormatter,
    "/ngr/mets/temperature_voltage": data_processing.Ads1115VoltageFormatter,
    "/ngr/mets/mets_adc_driver/P2_GND/voltage": data_processing.Ads1115VoltageFormatter,
    "/rosout": data_processing.RosoutFormatter,
}

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "mission", type=str, help="Mission name; will be prefix for output txt files"
    )
    args = parser.parse_args()
    data_processing.extract_data(output_filenames, topic_formatters, args.mission)
